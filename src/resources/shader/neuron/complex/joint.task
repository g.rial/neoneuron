#version 460
#extension GL_EXT_mesh_shader: enable

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

layout (set = 0, binding = 0) uniform Matrices {
    mat4 view;
    mat4 viewProjection;
    mat4 inverseProjection;
    vec4 planes[6];
    float near;
    float far;
};

layout (set = 0, binding = 1) uniform GlobalData {
    float currentTime;
    float radiusStrength;
    float startClip;
    float endClip;
    float splitHeight;
    float splitArcStrength;
    uint rotationIndexOffset;
    uint childrenRotationIndexOffset;
    uint minChildrenForJoint;
    uint verticesPerCircle;
    uint somaLatitudes;
    uint somaLongitudes;
    float somaConnectionPushFactor;
    float somaWeightPower;
    float somaSphereWeight;
    float somaConnectionMaxWeight;
    vec4 defaultColor;
    vec4 selectedColor;
    uint lod;
    uint frame;
    uint savingNeuron;
};

layout (set = 0, binding = 2) uniform Scene {
    vec3 sceneCenter;
    vec3 sceneRadius;
    uint sectionsAmount;
};

struct Section {
    uint neuronIndex;
    uint sectionId;
    uint metadata;
    uint parent;// This is the index inside the sections array! It is not the id of the section.
    vec4 endAndRadius;
};

struct Joint {
    uint parent;
    uint amount;
    uint rotationIndex;// Rotation index data will always be 0-15!
    uint connection[8];
};

struct Neuron {
    uint neuronId;
    uint lodMode;// 0-7 = forced, 8 = dynamic, 9 = static
    uint updateFrame;
    uint maxSegmentId;
    float radius;
    mat4 model;
    mat4 normal;
};

layout(std430, set = 2, binding = 0) buffer Neurons {
    Neuron neurons[];
};

layout(std430, set = 2, binding = 1) buffer Data {
    Section sections[];
};

layout(std430, set = 2, binding = 2) buffer Joints {
    Joint joints[];
};

struct TaskData {
    uint instance;
};

taskPayloadSharedEXT TaskData td;

uint computeLOD(Neuron neuron, Section segment) {
    if (neuron.lodMode == 8) {
        float sceneRadius = neuron.radius;
        vec3 viewPosition = vec3(view * neuron.model * vec4(segment.endAndRadius.xyz, 1.0f));
        float distance = length(viewPosition);
        return 7 - min(uint(distance * 7.0f / sceneRadius), 7);
    } else {
        return neuron.lodMode < 8 ? neuron.lodMode : lod;
    }
}

uint calculateEmit() {
    Joint joint = joints[gl_WorkGroupID.x];
    Section parent = sections[joint.parent];
    Neuron neuron = neurons[parent.neuronIndex];

    uint localLOD = computeLOD(neuron, parent);

    uint parentLOD = (parent.metadata >> 11u) & 7u;
    if (parentLOD > localLOD) return 0;
    if (joint.amount >= minChildrenForJoint) return 1;
    return 0;
}

void main() {
    td.instance = gl_WorkGroupID.x;
    EmitMeshTasksEXT(calculateEmit(), 1, 1);
}
    